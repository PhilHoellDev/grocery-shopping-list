$(document).ready(function () {
    $('.navbar-sidenav [data-toggle="tooltip"]').tooltip({
      template: '<div class="tooltip navbar-sidenav-tooltip" role="tooltip" style="pointer-events: none;"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
    })

    var pusher = new Pusher('a84348c24cb3216cb532', {
      cluster: 'ap3',
      encrypted: true
    });

    var orderChannel = pusher.subscribe('testorder');
    orderChannel.bind('my-event', function(data) { 
      var message = document.getElementById('message-count')
      var date = new Date();
      var toAppend = document.createElement('a')
      toAppend.classList.add('list-group-item', 'list-group-item-action')
      toAppend.href = '#'
      document.getElementById('message-box').appendChild(toAppend)
      toAppend.innerHTML ='<div class="media">'+
                      '<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">'+
                      '<div class="media-body">'+
                        `<strong>${data.variant}</strong> posted a new message `+
                        `<em>${data.quantity}</em>.`+
                        `<div class="text-muted smaller">Today at ${date.getHours()} : ${date.getMinutes()}</div>`+
                      '</div>'+
                    '</div>'

      message.innerText = parseInt(message.innerText)+1
    });
    });
});