Pusher.logToConsole = true;

// Configure Pusher instance
var pusher = new Pusher('a84348c24cb3216cb532', {
  cluster: 'ap3',
  encrypted: true
});

// Subscribe to poll trigger
var orderChannel = pusher.subscribe('order');

// Listen to 'order placed' event
var order = document.getElementById('order-count')
orderChannel.bind('place', function(data) {
  order.innerText = parseInt(order.innerText)+1
});