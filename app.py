### Server File ###

### Importing dependencies ###
from flask import Flask, render_template, request, jsonify
import sqlite3, datetime
from flask_socketio import SocketIO, emit
from threading import Thread
import time
import datetime
from operator import itemgetter

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'
socketio = SocketIO(app)


### Definition of web sockets ###
# General
@socketio.on('connect')
def handle_connect():
    print('Server: Client connected')

@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')

@socketio.on('recipes choosen')
def log_eating(args):
    connectionObject = sqlite3.connect("recipes.db")
    cursorObject = connectionObject.cursor()
    today = datetime.date.today()
    for arg in args.items():
        log_consumtion = '''INSERT INTO consumed values(?,?,?)'''
        cursorObject.execute(log_consumtion,(None,int(arg[1]),today))

    connectionObject.commit()

# Recipe Page
@app.route('/')
def index():
    connectionObject = sqlite3.connect("recipes.db")
    print("connected")
    cursorObject = connectionObject.cursor()
    queryTable = "SELECT * FROM recipes"
    queryResults = cursorObject.execute(queryTable)
    Recipes = cursorObject.fetchall()
    return render_template('index.html', Recipes=Recipes)


# Overview over cookings
@app.route('/consume',  methods=['POST'])
def overview():
    connectionObject = sqlite3.connect("recipes.db")
    print("connected")
    cursorObject = connectionObject.cursor()
    recipe_list = []
    data = request.form
    data = list(data.values())
    
    for number in range(len(data)):
        select_ingredients = '''SELECT Ingredients, Source, WPage FROM recipes WHERE ID = ?'''
        queryResults = (cursorObject.execute(select_ingredients, (data[number],)))
        result = cursorObject.fetchall()
        recipe_list.append(result)

    print(recipe_list)


    return render_template('consume.html', recipe_list = recipe_list)

### main method ###
if __name__ == '__main__':
        is_venv()  
        socketio.run(app)